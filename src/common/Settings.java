
package common;

/**
 * Stores global settings.
 */
public class Settings {

    public static int PORT = 6666;
    public static String QUIT_COMMAND = "quit";
    public static String LIST_CLIENTS = "ls";

    public static String YES = "yes";
    public static String NO = "no";

}
